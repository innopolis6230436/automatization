import json,datetime
import requests
import re
import threading
import aiohttp
import asyncio
import numpy as np
Sema = threading.BoundedSemaphore()
WeatherArr = []

class aobject(object):
    # непонятен смысл выражения, нашел на stackowerflow
    async def __new__(cls, *a, **kw):
        instance = super().__new__(cls)
        await instance.__init__(*a, **kw)
        return instance

    async def __init__(self):
        pass

class Weather(aobject):
    
    async def __init__(self, CityName:str,Session) -> None:
        
        City = re.sub(" ","%20",CityName)
        
        async with Session.get(f'https://api.openweathermap.org/data/2.5/weather?q={City}&appid=c9c85b3f755936c21a67b01f0a87c4b2&units=metric') as r:
            
            print("__"+CityName)

            js = await r.json()

            self.city_name = CityName
            self.humidity = js['main']['humidity']
            self.pressure = js['main']['pressure']
            self.temp = js['main']['temp']
            self.wind = js['wind']["speed"]
            self.__sunrise = datetime.datetime.fromtimestamp(js['sys']['sunrise'])
            self.__sunset = datetime.datetime.fromtimestamp(js['sys']['sunset'])
            self.day_length = self.__sunset - self.__sunrise

def GetCities(FileName:str) -> list:
    
    Ret = []
    
    with open(FileName, "r",encoding="utf-8") as File:

        pattern = r"[a-z]+"
    
        for idx, Line in enumerate(File):
        
            if idx == 0:
                continue

            City = re.findall(pattern,Line,re.IGNORECASE)
        
            if City != None :
            
                Ret.append(' '.join(City))
    
    return Ret

async def Worker(CityName:str,session) -> None:
    
    it = await Weather(CityName,session)
    Sema.acquire()
    WeatherArr.append(it)
    Sema.release()
    
async def GetAsyncWeather():
    
    CityList = GetCities("cities.txt")
    
    if len(CityList) == 0:
        raise RuntimeError("Bad cities file")
    
    Tasks = []

    async with aiohttp.ClientSession() as session:
        
        for it in CityList:
            T = asyncio.create_task(Worker(it,session,))
            Tasks.append(T)
        for it in Tasks:
            await it
    

        

if __name__ == '__main__':
    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(GetAsyncWeather())
    loop.close()
    for it in WeatherArr:
        print(it.city_name)

    print("Complite")